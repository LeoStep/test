//
//  main.c
//  test
//
//  Created by Леонид Степанчук on 01.10.2020.
//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>


#define WR  "a+t"
#define R   "r+t"
#define CHAR_END    '\n'

char path[] = {"/Users/leo/Downloads/extra_words.txt"};
static FILE *_filequeue;

/*
 *
 */
long io_get_size_file(void){
    if(_filequeue != NULL){
        fseek (_filequeue, 0, SEEK_END);
        return ftell (_filequeue);
    }else{
        return 0;
    }
}

/**
 
 */
int main(int argc, const char * argv[]) {
    
    _filequeue = fopen(path, R);
    if(_filequeue == NULL){
        printf("no file\n");
        return 1;
    }
    
    long size = io_get_size_file();
    if(size == 0){
        fclose(_filequeue);
        printf("empty file\n");
        return 1;
    }
    
    char * buffer;
    buffer = (char*) malloc (size);
    if(buffer == NULL){
        fclose(_filequeue);
        printf("error malloc\n");
        return 1;
    }

    printf("size = %d byte\n", (int)size);
    
    if(fseek (_filequeue, 0 ,SEEK_SET) != 0){
        free (buffer);
        fclose(_filequeue);
        printf("error seek\n");
        return 1;
    }

    long sta = fread((char*)buffer, 1, size, _filequeue);
    if(sta <= 0){
        free (buffer);
        fclose(_filequeue);
        printf("error read\n");
        return 1;
    }

    fclose(_filequeue);
    
    int count_end = 0;
    for(int i = 0; i < size; i++){
        if(buffer[i] == (char)CHAR_END){
            count_end++;
        }
    }

    printf("%s\n", buffer);
    printf("Success! size = %d byte, words = %d\n", (int)size, count_end);
    
    free (buffer);
    
    return 0;
}
